<?php
/**
 * Encuentra y devuelve los elementos repetidos en un array.
 *
 * @param array $array El array en el que buscar los elementos repetidos.
 * @param bool $devolverTodos Opcional. Determina si se deben devolver todos los elementos repetidos o solo la primera ocurrencia. El valor predeterminado es false.
 * @return array Devuelve un array que contiene los elementos repetidos.
 */


function elementosRepetidos($array, $devolverTodos = false)
{
    $repeated = array();

    foreach ((array) $array as $value) {
        $inArray = false;
        foreach ($repeated as $i => $rItem) {
            if ($rItem['value'] === $value) {
                $inArray = true;
                ++$repeated[$i]['count'];
            }
        }
        if (false === $inArray) {
            $i = count($repeated);
            $repeated[$i] = array();
            $repeated[$i]['value'] = $value;
            $repeated[$i]['count'] = 1;
        }
    }
    if (!$devolverTodos) {
        foreach ($repeated as $i => $rItem) {
            if ($rItem['count'] > 1) {
                unset($repeated[$i]);
            }
        }
    }
    sort($repeated);
    return $repeated;
}

$a=["lunes","martes","miercoles","lunes","jueves","viernes","sabado","domingo","martes"];

$salida=elementosRepetidos($a, true);
var_dump($salida);