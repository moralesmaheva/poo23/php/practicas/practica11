<?php
// funcion que acepte como argumentos valor minimo, valor maximo 
// numero de elementos y el array donde se almacenaran los valores

/**
 * Genera un array de números aleatorios dentro de un rango dado.
 *
 * @param int $vmin el valor mínimo para los números aleatorios
 * @param int $vmax el valor máximo para los números aleatorios
 * @param int $num la cantidad de números aleatorios a generar
 * @param array $array un array vacío para almacenar los números generados
 * @return void
 */

function arrayNumeros2(int $vmin, int $vmax, int $num, array $array)
{
    $array = [];
    for ($c = $vmin; $c <= $num; $c++) {
        $array[$c] = rand($vmin, $vmax);
    }
}
