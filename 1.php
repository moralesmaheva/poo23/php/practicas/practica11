<?php
// crear funcion que acepte como parametros valor minimo, valor maximo y numero de valores
//debe generar un array con numeros aleatorios que devuelve de forma ascendente

/**
 * Genera un array de números aleatorios dentro de un rango dado y devuelve el array ordenado.
 *
 * @param int $vmin El valor mínimo del rango.
 * @param int $vmax El valor máximo del rango.
 * @param int $num La cantidad de elementos a generar en el array.
 * @return array El array ordenado de números aleatorios.
 */

function arrayNumeros(int $vmin, int $vmax, int $num): array
{
    $numeros = [];
    for ($c = $vmin; $c <= $num; $c++) {
        $numeros[$c] = rand($vmin, $vmax);
    }
    sort($numeros);
    return ($numeros);
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 1</title>
</head>

<body>
    <?php
    $numeros = arrayNumeros(1, 10, 5);
    var_dump($numeros);
    ?>
</body>

</html>