<?php
// crear una funcion que nos permita crear un color aleatorio en RGB
// debemos pasarle el numero de colores a generar y un booleano que si es true nos
//coloque # y si es false no la coloque
//devuelve un array con los colores

/**
 * Genera un array de colores RGB aleatorios.
 *
 * @param int $numero El número de colores a generar.
 * @param bool $simbolo Si se debe incluir el símbolo "#" en cada color.
 * @return array El array de colores RGB aleatorios.
 */

function coloresRGB2(int $numero, bool $simbolo): array
{
    for ($c = 0; $c < $numero; $c++) {
        if ($simbolo == true) {
            $colores[$c] = "#";
        } else {
            $colores[$c] = "";
        }
        for ($c2 = 1; $c2 < 7; $c2++) {
            $colores[$c] .= dechex(rand(0, 15));
        }
    }
    return $colores;
}

$colores = coloresRGB2(5, false);
$colores2 = coloresRGB2(5, true);
var_dump($colores, $colores2);
