<?php
// funcion que nos permita crear un color aleatorio en RGB
//debemos pasarle el numero de colores a generar y devuelve un array con los colores

// usamos la funcion dechex() para convertir el numero decimal en hexadecimal

/**
 * Genera un array de colores RGB.
 *
 * @param int $numero El número de colores a generar.
 * @return array Un array de colores RGB.
 */

function coloresRGB(int $numero): array
{
    for ($c = 0; $c < $numero; $c++) {
        $colores[$c] = "#";
        for ($c2 = 1; $c2 < 7; $c2++) {
            $colores[$c] .= dechex(rand(0, 15));
        }
    }
    return $colores;
}

$colores = coloresRGB(5);
var_dump($colores);
